#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::string;
using std::vector;

struct Proizvod {
    string naziv;
    double cijena;
};

void ubaci(vector<Proizvod>& proizvodi, string naziv, double cijena) {
    proizvodi.push_back({ naziv, cijena });
}

double ukupno(const vector<Proizvod>& proizvodi) {
    double ukupnaCijena = 0;
    for (auto proizvod : proizvodi) {
        ukupnaCijena = ukupnaCijena + proizvod.cijena;
    }
    return ukupnaCijena;
}

int main() {
    vector<Proizvod> proizvodi;
    ubaci(proizvodi, "Napolitanke", 19.99);
    ubaci(proizvodi, "Mlijeko", 5.49);
    cout << ukupno(proizvodi);
}
