#include <iostream>

using std::cout;

int prebroji(int polje[], int n, int k, int m) {
    if (n > 0) {
        if (polje[n - 1] >= k && polje[n - 1] <= m) {
            return 1 + prebroji(polje, n - 1, k, m);
        } else {
            return prebroji(polje, n - 1, k, m);
        }
    }
    return 0;
}

int main() {
    int polje[5] = { 3, 4, 5, 6, 7 };
    int n = 5;
    cout << prebroji(polje, n, 4, 6);
}
