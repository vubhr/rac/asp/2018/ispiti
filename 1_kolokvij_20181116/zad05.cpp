#include <iostream>

using std::cout;

int suma(int n) {
    if (n > 0) {
        if (n % 3 == 0) {
            return n + suma(n - 1);
        } else {
            return suma(n - 1);
        }
    }
    return 0;
}

int main() {
    cout << suma(6);
}
