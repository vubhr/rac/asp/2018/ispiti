#include <iostream>
#include <string>

using std::cout;
using std::string;

class Knjiga {
public:
    Knjiga() {
        godinaIzdanja = 2018;
        cijena = 9.99;
    }
    Knjiga(string naslov, string autor, int godinaIzdanja, double cijena) {
        this->naslov = naslov;
        this->autor = autor;
        this->godinaIzdanja = godinaIzdanja;
        this->cijena = cijena;
    }
    string prikazi() {
        return autor + " - " + naslov;
    }
private:
    string naslov;
    string autor;
    int godinaIzdanja;
    double cijena;
};

int main() {
    Knjiga knjiga1;
    Knjiga knjiga2("ASP", "Kusalić D.", 2016, 119.0);
    cout << knjiga2.prikazi();
}
